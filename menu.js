
const menuTemplate = (fns, vars) =>{

    return [
        {
            label: 'File',
            submenu: [
                {
                    label: "Quit",
                    accelerator: process.platform === 'darwin' ? "Command+Q" : "Ctrl+Q",
                    click() { 
                        vars.app.quit();
                    }
                }
            ],
        },
        {
            
            label: 'Add',
            submenu: [
                {
                    label: "Add New Moveset",
                    click() {
                        fns.createNewAddWindow();
                    }
                },
                {
                    label: "Add From Showdown",
                },
                {
                    label: "Add From JSON",
                    click() {
                        fns.addJSONData();
                    }
                }
            ]
        },
        {
            label: 'Export',
            submenu: [
                {
                    label: "Export to Showdown",
                    click() {
                        fns.createSDExportWindow();
                    }
                },
                {
                    label: "Export to JSON",
                    click() {
                        fns.exportJSONData();
                    }
                }
            ]
        }
    ];
};

exports.menuTemplate = menuTemplate;