//Import modules and global varriables
const electron = require("electron");
const {app, BrowserWindow, Menu, ipcMain, dialog } = electron;
const fs = require("fs");

var fns = { 
    createNewAddWindow: createNewAddWindow,
    createSDExportWindow: createSDExportWindow,
    exportJSONData: exportJSONData,
    addJSONData: addJSONData
};
var variables = {app: app};

const menuTemplate = require("./menu").menuTemplate(fns, variables);

const MasterList = require("./Lib/MasterList").MasterList;

//reserve window variables
let mainWindow;
let addWindow;
let importWindow;
let exportWindowJSON;
let exportWindowSD;

var masterList = new MasterList(`${__dirname}/masterList.json`);

//what to do when app is ready.
app.on('ready', () => {

    mainWindow = new BrowserWindow({title: "Moveset Helper", icon: __dirname + '/icon.ico'});
    mainWindow.loadURL(`file://${__dirname}/views/index.html`);
    mainWindow.on('closed', () => app.quit());

    const mainMenu = Menu.buildFromTemplate(menuTemplate);
    Menu.setApplicationMenu(mainMenu);

});

ipcMain.on("mainWindow:getList", (event) => {
    masterList.refresh();
    mainWindow.webContents.send("mainWindow:populate", masterList.exportSD());
})

ipcMain.on("addNew:add", (event, msg) => {
    masterList.add(msg);
    mainWindow.webContents.send("mainWindow:update", masterList.exportSD());
    addWindow.close();
});

ipcMain.on("exportSD:ready", (event) => {
    exportWindowSD.webContents.send("exportSD:sendData", masterList.exportSD());
})

function exportJSONData() {

    dialog.showSaveDialog({
        title: "Export JSON", 
        filters: [{
            name: "JSON Data",
            extensions: ["json"]
        }]},
        
        (fileName) => {

        if (fileName === undefined) return;

        fs.writeFile(fileName, masterList.exportJSON(), (err) => {

        });
    });
}

function addJSONData(){
    dialog.showOpenDialog({
        title: "Add From JSON",
        filters: [{
            name: "JSON Data",
            extensions: ["json"]
        }]
    }, (fileNames) => {

        if(fileNames === undefined) return;

        var fileName = fileNames[0];

        fs.readFile(fileName, "utf-8", (err, data) => {

            var newList = JSON.parse(data);
            for(var i = 0; i < newList.movesets.length; i++){
                masterList.add(newList.movesets[i])
            }

            mainWindow.webContents.send("mainWindow:update", masterList.exportSD());

        })

    });
}

function createSDExportWindow() {
    exportWindowSD = new BrowserWindow({
        icon: __dirname + '/icon.ico',
        maximizable: false,
        resizable: false,
        width: 400,
        height: 500,
        title: "Export Showdown"
    });
    exportWindowSD.loadURL(`file://${__dirname}/views/exportSD.html`);
    exportWindowSD.setMenu(null);
    exportWindowSD.on("close", () => exportWindowSD = null);
}

function createNewAddWindow() {
    addWindow = new BrowserWindow({
        icon: __dirname + '/icon.ico',
        maximizable: false,
        resizable: false,
        width: 496,
        height: 620,
        title: "Add Moveset"
    });
    addWindow.loadURL(`file://${__dirname}/views/addNew.html`);
    addWindow.setMenu(null);
    addWindow.on("close", () => addWindow = null);
}

//cross platform shit.
if(process.platform == "darwin"){
    menuTemplate.unshift({});
}

//Make sure we can only see the dev tools tab when we are in development.
if(process.env.NODE_ENV !== "production"){
    menuTemplate.push({
        label: "Developer",
        submenu: [
            {
                label: "Toggle Dev Tools",
                accelerator: process.platform === 'darwin' ? "Command+Alt+I" : "Ctrl+Shift+I",
                click(item, focusedWindow) {
                    focusedWindow.toggleDevTools();
                }
            },
            {
                role: "reload"
            }
        ]
    })
}