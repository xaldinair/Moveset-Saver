$(document).ready( function() {

    function populate(list){
        for(var i = 0; i < list.length; i++){
            $("#movelist").append(
                `<div class="col-sm-6" style="white-space: pre-wrap">${list[i]}</div>`
            );
        }
    }

    const electron = require("electron");
    const {ipcRenderer} = electron;
    
    ipcRenderer.send("mainWindow:getList");

    ipcRenderer.on("mainWindow:populate", (event, list) => {
        populate(list)
    })

    ipcRenderer.on("mainWindow:update", (event, list) => {
        $("#movelist").html("");
        populate(list);
    })
    
});