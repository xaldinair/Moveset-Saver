$(document).ready(function() {

    const electron = require("electron");
    const {ipcRenderer} = electron;

    ipcRenderer.send("exportSD:ready");

    ipcRenderer.on("exportSD:sendData", (event, exportData) => {
        for(var i = 0; i < exportData.length; i++){
            $("#exportWindow").append(`<div style="white-space: pre-wrap">${exportData[i]}</div><br/>`);
        }
    });



});